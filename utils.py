import os
from datetime import datetime

class Logger:
    def __init__(self, logdir, experiment_note, folder_name='./experiments'):
        today = datetime.now().strftime("%Y-%m-%d")
        time = datetime.now().strftime("%H:%M")

        folder_n = folder_name

        self.path = os.path.join(folder_n,today, logdir+'_'+time)

        if not os.path.exists('./'+today):
            os.makedirs(self.path, exist_ok=True)


        with open(os.path.join(self.path, 'experiment_info.txt'), 'w') as f:
            f.write(experiment_note+'\n')

    def record(self, note):
        with open(os.path.join(self.path, 'scalars.txt'),'a') as f:
            f.write(note+'\n')
