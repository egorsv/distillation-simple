import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR
#from torch.autograd import Variable

class KdSimple:
    def __init__(self, cuda=True, temperature=1, alpha=1, logger=None):
        self.cuda=cuda
        self.T=temperature
        self.alpha=alpha
        self.logger = logger

    def train_student(self,
                      student,
                      teacher,
                      train_loader,
                      optimizer):

        student.train()
        # model.eval() will set all model layers to eval mode,
        # that way, batchnorm or dropout layers will work in eval mode
        # instead of training mode.
        teacher.eval()

        for train_batch, labels_batch in train_loader:
            # move to cuda if avaliable
            if self.cuda:
                train_batch = train_batch.cuda()
                true_labels = labels_batch.cuda()

            with torch.no_grad():
                # impacts the autograd engine and deactivate it.
                # it will reduce memory usage and speed up computations
                # but you won’t be able to backprop
                teacher_labels = teacher(train_batch)

            student_labels = student(train_batch)

            # compute loss
            loss = self.kd_loss(student_labels,
                                teacher_labels,
                                true_labels)

            # clear previous gradients,
            # compute gradients of all variables wrt loss
            optimizer.zero_grad()
            loss.backward()
            # performs updates using calculated gradients
            optimizer.step()
        return loss.item()

    def train_and_eval(self,
                       student,
                       teacher,
                       train_loader,
                       validation_loader,
                       optimizer,
                       epochs = 10):

        # decayed by gamma every step_size=10 by gamma
        # Assuming optimizer uses lr = 0.05 for all groups
        # lr = 0.05     if epoch < 30
        # lr = 0.005    if 30 <= epoch < 60
        # lr = 0.0005   if 60 <= epoch < 90

        scheduler = StepLR(optimizer, step_size=20, gamma=0.5)

        for epoch in range(epochs):
            train_loss = self.train_student(student,
                               teacher,
                               train_loader,
                               optimizer)
            scheduler.step()

            val_accuracy = self.student_eval(student, validation_loader)
            if  self.logger is not None:
                self.logger.record(f'{train_loss}, {val_accuracy }')
            print(f'loss: {train_loss}, accuracy: {val_accuracy}')


    def kd_loss(self, student_labels, teacher_labels, true_labels):

        """
        Compute the knowledge-distillation (KD) loss given outputs, labels.

        NOTE: Using KL Divergence instead of cross entropy as in original paper.
        H(p,q) = H(p) + KL(p || k)  and  H(p) = const.

        from here : https://github.com/peterliht/knowledge-distillation-pytorch/issues/2

        "Hyperparameters": temperature and alpha

        NOTE: PyTorch implementation of KLDivLoss
        expects the input as log-probabilities and
        the targets as probabilities

        P(s) = softmax(Xs/T) - student
        P(t) = softmax(Xt/T) - teacher
        KD_loss = alpha*T^2*KL( P(s)*ln(P(s)/P(q))) + (1-alpha)*-sum(true_labels * ln(P(s)))
        """

        kl = nn.KLDivLoss()(F.log_softmax(student_labels/self.T, dim=1),
                            F.softmax(teacher_labels/self.T, dim=1))
        cross_ent = F.cross_entropy(student_labels, true_labels)
        KD_loss = (self.alpha*self.T**2)*kl + (1. - self.alpha)*cross_ent

        return KD_loss

    def student_eval(self, net, dataloader):
        # add other metrics if needed
        net.eval()
        correct = 0
        total = 0
        with torch.no_grad():
            for data in dataloader:
                images, labels = data
                images = images.cuda()
                labels = labels.cuda()
                outputs = net(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
        return 100 * correct / total
